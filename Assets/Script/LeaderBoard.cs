using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class LeaderBoard : MonoBehaviour
{
    [SerializeField] private List<string> _nameBot = new List<string>() { "jack", "jck", "kcj", "j4ck", "best" };
    [SerializeField] private List<float> _scoreBot = new List<float>() { 5000, 4000, 3000, 2000, 500};
    [SerializeField] private List<TextMeshProUGUI> _nameBotText = new List<TextMeshProUGUI>();
    [SerializeField] private List<TextMeshProUGUI> _scoreBotText = new List<TextMeshProUGUI>();
    // Start is called before the first frame update
    private float _scorePlayer;
    void Start()
    {
        _scorePlayer = PlayerPrefs.GetFloat("HighestScore");
        for (int i = 0; i < _scoreBot.Count; i++)
        {
            if (_scoreBot[i] < _scorePlayer)
            {
                _scoreBot.Insert(i,_scorePlayer);
                _nameBot.Insert(i,"Player");
                _scoreBot.RemoveAt(_scoreBot.Count);
                _nameBot.RemoveAt(_nameBot.Count);
                break;
            }
        }
        // for (int i = 0; i < _nameBot.Count; i++)
        // {
        //     _nameBotText.Add(_nameBot[i].ToString());            
        // }
    }
}
