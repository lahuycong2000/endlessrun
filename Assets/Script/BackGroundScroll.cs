using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BackGroundScroll : MonoBehaviour
{
    [SerializeField] private GameController _gameController;
    private Vector3 _startPos;
    private float _repeatWidth;
    void Start()
    {
        _startPos = transform.position;
        _repeatWidth = GetComponent<BoxCollider2D>().size.x;
    }

    void Update()
    {
        if (transform.position.x < _startPos.x - _repeatWidth)
        {
            transform.position = _startPos;
        }
        if (_gameController.IsPlayGame && !_gameController.IsEndGame)
        {
            transform.Translate(Vector3.left * Time.deltaTime * _gameController.SpeedBackGround);
        }
    }
}

