using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private GameController _gameController;
    public GameController GameManager { get => _gameController; set => _gameController = value; }

    void Update()
    {
        if (!_gameController.IsEndGame)
        {
            transform.Translate(Vector2.left * (_gameController.SpeedEmemy * 2) * Time.deltaTime);
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Generate"))
        {
            _gameController.InstantiateEnemy();
            Debug.Log("gen");
        }
        if (other.gameObject.CompareTag("End"))
        {
            Destroy(this.gameObject);
            Debug.Log("end");
        }
    }
}
