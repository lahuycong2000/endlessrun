using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
public class GameController : MonoBehaviour
{
    [SerializeField] private List<GameObject> _prefabEnemy = new List<GameObject>();
    [SerializeField] private PlayerController _playController;
    [SerializeField] private TextMeshProUGUI _scorePlayer;
    [SerializeField] private TextMeshProUGUI _scoreLast;
    [SerializeField] private TextMeshProUGUI _highScore;
    [SerializeField] private bool _isPlayGame;
    [SerializeField] private bool _isEndGame;
    [SerializeField] private Button _buttonPlayGame;
    [SerializeField] private Button _buttonEndGame;
    [SerializeField] private GameObject _menuGame;
    [SerializeField] private GameObject _endGame;
    [SerializeField] private GameObject _generateEnemy;


    private float _speedEmemy;
    private float _scoreCurrent;
    private float _scorePoint;
    private float _scorePointCurrent;
    public float _speedBackGround;
    private float _numberScore;

    public bool IsPlayGame { get => _isPlayGame; set => _isPlayGame = value; }
    public bool IsEndGame { get => _isEndGame; set => _isEndGame = value; }
    public float SpeedBackGround { get => _speedBackGround; set => _speedBackGround = value; }
    public float SpeedEmemy { get => _speedEmemy; set => _speedEmemy = value; }

    public void Awake()
    {
        if (PlayerPrefs.GetInt("Retry") == 1)
        {
            _menuGame.gameObject.SetActive(true);
        }
        PlayerPrefs.SetInt("Retry", 0);
    }
    void Start()
    {
        _endGame.gameObject.SetActive(false);
        _scoreCurrent = 0;
        _speedBackGround = 5f;
        _scorePoint = 100f;
        _scorePointCurrent = 100f;
        _numberScore = 0.5f;
        _isPlayGame = false;
        _isEndGame = false;
        _speedEmemy = 5;
    }

    void OnEnable()
    {
        _buttonPlayGame.onClick.AddListener(StartGame);
        _buttonEndGame.onClick.AddListener(ResetGame);
    }
    public void StartGame()
    {
        _playController.Animator.SetBool("isRunning", true);
        _menuGame.gameObject.SetActive(false);
        _isPlayGame = true;
        InstantiateEnemy();
    }

    public void InstantiateEnemy()
    {
        int index;
        int random = Random.Range(1, 10);
        if (random % 2 == 0)
        {
            index = 0;
        }
        else
        {
            index = 1;
        }
        Vector3 vector = new Vector3(_generateEnemy.transform.position.x, _prefabEnemy[index].transform.position.y, _generateEnemy.transform.position.y);
        GameObject ememy = Instantiate(_prefabEnemy[index], vector, transform.rotation);
        ememy.GetComponent<Enemy>().GameManager = this;
    }
    void Update()
    {
        if (_isPlayGame && !_isEndGame)
        {
            if (_scoreCurrent > _scorePointCurrent)
            {
                _scorePointCurrent += _scorePoint;
                _speedBackGround += _numberScore;
                _speedEmemy += _numberScore;
            }
            _scoreCurrent += 0.1f;
            _scorePlayer.text = _scoreCurrent.ToString("0");
        }
    }
    public void EndGame()
    {
        _endGame.gameObject.SetActive(true);
        _isEndGame = true;
        _scoreLast.text = _scoreCurrent.ToString("0");
        PlayerPrefs.SetInt("Retry", 1);
        if (_scoreCurrent > PlayerPrefs.GetFloat("HighestScore"))
        {
            PlayerPrefs.SetFloat("HighestScore", _scoreCurrent);
        }
        _highScore.text = PlayerPrefs.GetFloat("HighestScore").ToString();
    }
    public void ResetGame()
    {
        SceneManager.LoadScene("SceneGame");
    }

}
