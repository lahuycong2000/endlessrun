using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private GameController _gameController;
    [SerializeField] private Animator _animator;
    [SerializeField] private Rigidbody2D _rigidbody2D;
    private float _jumpHigh;
    public Animator Animator { get => _animator; set => _animator = value; }
    void Start()
    {
        _jumpHigh = 400f;
    }

    void Update()
    {
        if (_gameController.IsPlayGame && !_gameController.IsEndGame)
        {
            if (Input.anyKey && _animator.GetBool("isJumping") == false)
            {
                _rigidbody2D.AddForce(new Vector2(_rigidbody2D.velocity.x, _jumpHigh));
                _animator.SetBool("isJumping", true);
                //Debug.Log("Jump");
            }
        }
    }
    public void OnCollisionEnter2D(Collision2D other)
    {
        // if (other.gameObject.CompareTag("Ground"))
        // {
            _animator.SetBool("isJumping", false);
            //Debug.Log("Down");
        //}
        if (other.gameObject.CompareTag("Enemy"))
        {
            _animator.SetBool("isDead", true);
            _gameController.EndGame();
            //Debug.Log("End");
        }
    }
}
